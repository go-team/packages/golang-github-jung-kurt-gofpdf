Source: golang-github-jung-kurt-gofpdf
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Andreas Tille <tille@debian.org>,
           Nilesh Patra <nilesh@debian.org>
Section: devel
Testsuite: autopkgtest-pkg-go
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-golang,
               golang-any,
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-jung-kurt-gofpdf
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-jung-kurt-gofpdf.git
Homepage: https://github.com/jung-kurt/gofpdf
Rules-Requires-Root: no
XS-Go-Import-Path: github.com/jung-kurt/gofpdf

Package: golang-github-jung-kurt-gofpdf-dev
Architecture: all
Depends: ${misc:Depends},
         ${shlibs:Depends}
Multi-Arch: foreign
Description: PDF document generator with high level support for text, drawing and images
 Package gofpdf implements a PDF document generator with high level
 support for text, drawing and images.  Features
  * UTF-8 support
  * Choice of measurement unit, page format and margins
  * Page header and footer management
  * Automatic page breaks, line breaks, and text justification
  * Inclusion of JPEG, PNG, GIF, TIFF and basic path-only SVG images
  * Colors, gradients and alpha channel transparency
  * Outline bookmarks
  * Internal and external links
  * TrueType, Type1 and encoding support
  * Page compression
  * Lines, Bézier curves, arcs, and ellipses
  * Rotation, scaling, skewing, translation, and mirroring
  * Clipping
  * Document protection
  * Layers
  * Templates
  * Barcodes
  * Charting facility
  * Import PDFs as templates
